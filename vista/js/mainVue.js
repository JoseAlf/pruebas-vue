new Vue({
	el:"#appFactura",
	data:{
		producto:{
			id: 0,
			nombre 	: '',
			precio 	: 0,
			cantidad: 0,
			iva:19
		},
		productos:[]
	},
	methods:{
		agregar: function(){
			let pr = Object.assign({},this.producto)
			this.productos.push(pr)
			this.producto = {}
		},
		actualizar: function(inex){
			this.$set(this.productos,this.producto.id, this.producto)  
			this.producto = {}
		},
		editar: function(index){
			let prs = Object.assign({},this.productos[index])
			prs.id = index
			this.producto = prs
		},
		eliminar: function(index){
            this.productos.splice(index, 1);
		}
	}
});