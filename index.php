<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Inventario Articulos - Vue-js</title>
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">	
	<link rel="stylesheet" href="vista/font-awesome/css/font-awesome.css">
	<link rel="stylesheet" href="vista/css/estilos.css">
	
</head>
<body>
	<header class="row justify-content-start">
		<div class="col-1 ">
			<div class="rounded-circle logo border border-warning">JT</div>
		</div>
		<div class="col-10 titulo">
			<h1>EJEMPLO DE CRUD PARA UN INVENTARIO DE ARTICULOS CON VUE.JS Y AXIOS</h1>
			<h3>Ing. Jose Alfredo Tapia Arroyo</h3>			
		</div>
	</header>
	<section id='appFactura' class='container-fluid'>
		<div class="row">		
			<div class="col-4">
				<div class="row">
					<div class="col-12">
						<div class="marcoFormulario">	
							<h4>DATOS DEL PRODUCTO</h4>
							<form @submit.prevent()>
								<label for="">Nombre</label>
								<input type="text" class="form form-control" id="nombres" v-model="producto.nombre">
								<label for="">Precio</label>
								<input type="number" class="form form-control" id="precio" v-model.number="producto.precio">
								<label for="">Cantidad</label>
								<input type="text" class="form form-control" id="cantidad" v-model.number="producto.cantidad">	
								<label for="">Iva</label>
								<input type="text" class="form form-control" id="iva" value="19%" v-model.number="producto.iva">	
							</form>
						</div>						
					</div>
				</div>
				<div class="row">
					<div class="col-lg-6">
						<button class="btn btn-primary boton" id="btnAgregar" @click="agregar">
							<i class="fa fa-plus"> </i> Agregar
						</button>					
					</div>
					<div class="col-lg-6">					
						<button class="btn btn-warning boton" @click="actualizar">
							<i class="fa fa-refresh"> </i> Actualizar
						</button>
					</div>
				</div>		
			</div>
			<div class="col-8 espacioTabla">
				<h2>Listado de productos registrados</h2>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>No.</th>
							<th>NOMBRE</th>
							<th>PRECIO</th>
							<th>CANTIDAD</th>
							<th>SUB TOTAL</th>
							<th>IVA</th>
							<th>ACCIONES</th>
						</tr>
					</thead>
					<tbody>
						<tr v-for="(reg, clave) in productos">
							<td>{{clave + 1}}</td>
							<td>{{reg.nombre}}</td>
							<td class="valor">$ {{reg.precio}}</td>
							<td>{{reg.cantidad}}</td>
							<td class="valor">$ {{reg.precio * reg.cantidad}}</td>						
							<td class="valor">$ {{(reg.precio * reg.cantidad)*(reg.iva/100)}}</td>
							<td>
								<i class="fa fa-pencil btn btn-info boton2"  @click="editar(clave)"></i>
								<i class="fa fa-trash btn btn-danger boton2" @click="eliminar(clave)"></i>
							</td>
						</tr>					
					</tbody>
				</table>		
			</div>
		</div>
	</section>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.21/vue.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>  
	<script src="vista/js/mainVue.js"></script>
</body>
</html>